package ansiblemodule

import "bitbucket.org/HelgeOlav/utils/version"

// Version number of package
const Version = "0.0.2"

func init() {
	version.AddModule(version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/ansiblemodule", Version: Version})
}

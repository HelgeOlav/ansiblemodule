package ansiblemodule

import (
	"reflect"
	"testing"
)

func TestAnsibleInventory_ProduceOutput(t *testing.T) {
	// make inventory
	inventory := MakeTestInventory()
	// produce
	result := inventory.ProduceOutput()
	meta := result.getMeta()
	if len(result) == 0 {
		t.Error("len=0 result")
	}
	// check that we have a server group "all_servers" with only one child in it
	if r1, ok := result["all_servers"]; ok {
		if t1, ok := r1.(ansibleGroupOutput); ok {
			if len(t1.Children) == 1 && len(t1.Hosts) == 0 {
				if t1.Children[0] != "servers" {
					t.Error("all_servers does not contain child servers as only child")
				}
			} else {
				t.Error("all_servers has wrong count of children and hosts:", t1)
			}
		} else {
			t.Error("all_servers of wrong type, found", reflect.TypeOf(r1))
		}
	} else {
		t.Error("Missing all_servers group")
	}
	// verify all group
	allRes := result.getGroup(ansibleAllGroup)
	if allRes != nil {
		if len(allRes.Hosts) != 0 && len(allRes.Children) != 4 {
			t.Error("All group has wrong content:", *allRes)
		}
	} else {
		t.Error("No all group")
	}
	// verify host1 vars
	host1Vars := meta.getKey("host1")
	if len(host1Vars) == 2 {
		if host1Vars["group"] != "servers" {
			t.Error("host1 missing var group;", host1Vars)
		}
	} else {
		t.Error("host1 vars wrong count, found", host1Vars)
	}
	// check result map items
	if len(result) != 6 {
		t.Error("Produce() map has wrong item count, got", len(result))
	}
}

func TestAnsibleInventory_serveHost(t *testing.T) {
	inventory := MakeTestInventory()
	// check host1
	result := inventory.serveHost("host1")
	if len(result) != 1 {
		t.Error("host1 invalid number of vars")
	}
	// check host3
	result = inventory.serveHost("host3")
	if len(result) != 0 {
		t.Error("host3 invalid number of vars")
	}
}

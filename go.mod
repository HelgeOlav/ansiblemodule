module bitbucket.org/HelgeOlav/ansiblemodule

go 1.16

require (
	bitbucket.org/HelgeOlav/utils v0.0.0-20211209011010-71743d09240c // indirect
	github.com/mitchellh/mapstructure v1.4.1
)

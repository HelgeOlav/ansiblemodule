# Ansible integration in Go

This is a framework to let you write Ansible modules and inventories in Golang.

## How modules work

Modules works as follows:

1. The executable is called with one parameter, name of a JSON file with input parameters. A struct AnsibleRequest contains the input from Ansible.
2. The response is sent to stdout. AnsibleReponse is the struct that is sent back. If module exits with an error code then it has failed.

Look at examples/helloworld for a module that takes an input and sends a result back.

Your code starts by calling ```req, err := ansiblemodule.NewAnsibleRequestFromFile(os.Args[1])```. If err == nil then you have a request object.

To get data you can use ```name := req.GetKeyAsString("name")``` to get a parameter called name from Ansible.
You can load a struct by calling GetKeyAsType.

## Where to put the compiled modules

The modules has to be installed in one of the following directories. (This was found in the Ansible [documentation](https://docs.ansible.com/ansible/latest/dev_guide/developing_locally.html).)

* Any folder in the environment variable ANSIBLE_LIBRARY.
* In the folder ~/.ansible/plugins/modules/
* In the folder /usr/share/ansible/plugins/modules/

## Inventories

You can pass an executable as -i to Ansible. Look at examples/sampleinventory on how to use this.
The basic idea here is that you populate ansiblemodule.AnsibleInventory with your hosts and then call AnsibleInventory.Serve()
to handle everything for you.
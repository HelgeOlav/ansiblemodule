package ansiblemodule

import (
	"encoding/json"
	"testing"
)

func TestAnsibleRequest_GetKeyAsType1(t *testing.T) {
	// make some data
	var myReq AnsibleRequest = make(map[string]interface{})
	myReq["variable1"] = "My variable"
	type Data struct {
		Number int
		Text   string
	}
	innerdata := Data{
		Number: 4,
		Text:   "my text",
	}
	myReq["data"] = innerdata
	// try get data back
	var myResponse Data
	err := myReq.GetKeyAsType("data", &myResponse)
	if err != nil {
		t.Error(err)
		return
	}
	if myResponse.Number != innerdata.Number {
		t.Error("Number was not parsed correctly")
	}
	if myResponse.Text != innerdata.Text {
		t.Error("Text was not parsed correctly")
	}
}

func TestAnsibleRequest_GetKeyAsType2(t *testing.T) {
	type Data struct {
		Number int    `json:"my_number"`
		Text   string `json:"my_text"`
	}
	innerdata := Data{
		Number: 6,
		Text:   "My Text",
	}
	bytes, err := json.Marshal(&innerdata)
	if err != nil {
		t.Error(err)
		return
	}
	// unmarshal
	req, err := NewAnsibleRequestFromBytes(bytes)
	if err != nil {
		t.Error(err)
		return
	}
	if req.GetKeyAsString("my_text") != innerdata.Text {
		t.Error("Text not matched")
	}
	if req.GetKeyAsInt("my_number") != innerdata.Number {
		t.Errorf("Number not matched, got %v", req.GetKeyAsInt("my_number"))
	}
}

func TestAnsibleRequest_GetKeyAsString(t *testing.T) {
	req := AnsibleRequest{"number": 3, "bool": true, "text": "string", "nothing": nil}
	if req.GetKeyAsString("number") != "3" {
		t.Error("did not get number as string")
	}
	if req.GetKeyAsString("bool") != "true" {
		t.Error("Bool failed, should have true, got", req.GetKeyAsString("bool"))
	}
	if req.GetKeyAsString("text") != "string" {
		t.Error("text failed")
	}
	if req.GetKeyAsString("nothing") != "" {
		t.Error("nil failed")
	}
}

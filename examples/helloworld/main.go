package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"io"
	"io/ioutil"
	"os"
)

// This module implements "hello world"
func main() {
	// First we will try to load the passed data from the command line. This will fail if no file is specified.
	req, err := ansiblemodule.NewAnsibleRequestFromFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	// during development, lets copy the input so we can look at it
	var copiedFile string
	{
		file, err := ioutil.TempFile("", "ansiblemodule-")
		if err == nil {
			source, err := os.Open(os.Args[1])
			if err == nil {
				io.Copy(file, source)
				copiedFile = file.Name()
				file.Close()
				source.Close()
			}
		}
	}
	// now we will try to find the name specified as an argument
	name := req.GetKeyAsString("name")
	if len(name) == 0 {
		name = "Ansible" // set a default name
	}
	// say hello
	name = "Hello, " + name + "!"
	// Now lets create a response
	type myResponseT struct {
		Text string `json:"text"`
		File string `json:"filename,omitempty"`
	}
	myReponse := myResponseT{
		Text: name,
		File: copiedFile,
	}
	ansiblemodule.SuccessResponse(true, myReponse)
	// we never reach this part as SuccessResponse also exits the program
}

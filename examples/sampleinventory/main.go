package main

import "bitbucket.org/HelgeOlav/ansiblemodule"

func main() {
	// First we need to make our inventory, this is something you have to make on your own.
	// In this sample I will use a test inventory from the main package. Have a look and see how it works.
	// If there is a file "inventory.json" in the working directory it will be loaded instead of the test inventory.
	inventory, err := ansiblemodule.NewInventoryFromFile("inventory.json")
	if err != nil {
		inventory = ansiblemodule.MakeTestInventory()
	}

	// If you don't have any special needs you can just call Serve() and it will handle everything for you.
	// Keep in mind that Serve() never returns and you should close any open resources before you get here.
	inventory.Serve()
}

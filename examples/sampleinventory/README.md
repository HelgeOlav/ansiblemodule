# sampleinventory

This program shows how to use ansiblemodule to produce an inventoryfile
you can use with Ansible.

This program looks for inventory.json and tries to use that file, if not it will
use a dummy builtin inventory.

Your main only needs to produce inventory in form of a data format and then call Serve() to respond back as an inventory.

```go
func main() {
	// first make an inventory
    inventory := ansiblemodule.AnsibleInventory{}
    // create a host
    host1 := ansiblemodule.AnsibleHost{
        Host:   "host1",
        Groups: []string{"servers", "clients"},
        Vars:   map[string]string{"foo": "bar"},
    }
    // add host to inventory
    inventory.Hosts = []ansiblemodule.AnsibleHost{host1}
    // and serve it
    inventory.Serve()
}
```

## Demo

Attached is a sample inventory.json, and an Ansible playbook ping.yml. Run it using

```bash
go build .
ansible-playbook -i sampleinventory ping.yml
```

This playbook only works on a group of hosts called "internet". It will first try to ping them and then download
its frontpage. Note that Ansible ping module actually don't ping anything, it is just a verification of that you have access
to a system. Since everything runs locally on your host it never fails.

Nested groups are supported. This is the same as [group:children] in an ordinary inventory file.

Group vars are not supported in dynamic inventories, but is achieved by applying group vars directly onto a host.
By this nesting of group vars does not work and if you try to apply the same variable several times it is not determined who is winning. 
package ansiblemodule

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

// Ansible inventories can be executables. Here is an implementation that you can use to create a dynamic inventory.
// https://docs.ansible.com/ansible/2.9/dev_guide/developing_inventory.html#developing-inventory
// ansible-inventory -i inventory.yaml --list

// AnsibleInventory is the top level struct for working with inventories
type AnsibleInventory struct {
	Hosts     []AnsibleHost                `json:"hosts"`              // list of hosts
	Groups    map[string][]string          `json:"nested_groups"`      // nested groups
	GroupVars map[string]map[string]string `json:"group_vars"`         // group vars, applied directly to each host in a group
	SkipMeta  bool                         `json:"skipMeta,omitempty"` // if true _meta is not returned
}

// AnsibleHost is a host in the inventory
type AnsibleHost struct {
	Host   string            `json:"host"`     // name of host
	Groups []string          `json:"memberOf"` // host groups that we are member of
	Vars   map[string]string `json:"vars,omitempty"`
}

// ansibleInventoryOutput top level Ansible inventory output
type ansibleInventoryOutput map[string]interface{}

// getGroup returns name as group if it exist, nil otherwise
func (a ansibleInventoryOutput) getGroup(name string) *ansibleGroupOutput {
	if res, ok := a[name]; ok {
		if t, ok := res.(ansibleGroupOutput); ok {
			return &t
		}
	}
	return nil
}

// getMeta returns metadata if it exist, nil otherwise
func (a ansibleInventoryOutput) getMeta() *ansibleMetaTop {
	if res, ok := a[ansibleInventoryMeta]; ok {
		if t, ok := res.(ansibleMetaTop); ok {
			return &t
		}
	}
	return nil
}

// ansibleGroupOutput is a group under ansibleInventoryOutput
type ansibleGroupOutput struct {
	Children []string `json:"children,omitempty"`
	Hosts    []string `json:"hosts,omitempty"`
}

const ansibleInventoryMeta = "_meta"
const ansibleUngrouped = "ungrouped"
const ansibleAllGroup = "all"

// ansibleMetaTop is the host vars under ansibleInventoryOutput
type ansibleMetaTop struct {
	HostVars map[string]map[string]string `json:"hostvars,omitempty"`
}

// getKey returns the map for the given key, nil if it does not exist
func (a *ansibleMetaTop) getKey(key string) map[string]string {
	if a != nil {
		if result, ok := a.HostVars[key]; ok {
			return result
		}
	}
	return nil
}

// ProduceOutput produces output for --list. The output has to be marshalled.
func (h *AnsibleInventory) ProduceOutput() ansibleInventoryOutput {
	result := make(ansibleInventoryOutput)
	groups := make(map[string]ansibleGroupOutput)
	var allGroup ansibleGroupOutput
	var hostVars ansibleMetaTop
	hostVars.HostVars = make(map[string]map[string]string)
	if h != nil {
		// go through all hosts
		for _, host := range h.Hosts {
			// now put them into right group
			if len(host.Groups) == 0 { // make sure the host is at least in the ungrouped group
				host.Groups = []string{ansibleUngrouped}
			}
			for _, group := range host.Groups {
				if newGroup, ok := groups[group]; ok {
					newGroup.Hosts = append(newGroup.Hosts, host.Host)
					groups[group] = newGroup
				} else {
					newGroup := ansibleGroupOutput{Hosts: []string{host.Host}}
					groups[group] = newGroup
				} // if groups[group]
			} // range host.Groups
			// check for hostvars
			if len(host.Vars) > 0 {
				hostVars.HostVars[host.Host] = host.Vars
			}
			// check for groupvars
			for _, group := range host.Groups {
				for gk, gv := range h.GroupVars {
					if group == gk {
						for k, v := range gv {
							if hostVars.HostVars[host.Host] == nil {
								hostVars.HostVars[host.Host] = make(map[string]string)
							}
							hostVars.HostVars[host.Host][k] = v
						}
					}
				}
			}
		} // range h.Hosts
		if h.SkipMeta == false {
			result[ansibleInventoryMeta] = hostVars
		}
		// handle nested groups, found in configuration
		for k, v := range h.Groups {
			if newGroup, ok := groups[k]; ok {
				newGroup.Children = append(newGroup.Children, v...)
				groups[k] = newGroup
			} else {
				newGroup := ansibleGroupOutput{}
				newGroup.Children = append(newGroup.Children, v...)
				groups[k] = newGroup
			}
		}
		// make the result
		for k, v := range groups {
			result[k] = v
			allGroup.Children = append(allGroup.Children, k)
		}
		result[ansibleAllGroup] = allGroup
	} // if h != nil
	return result
}

// Serve handles command line arguments, produces the output and exits the program
// --list (and no parameters) produce a complete list
// --host hostname produces only host vars for that host
func (h *AnsibleInventory) Serve() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) == 0 {
		h.serveList()
	}
	switch argsWithoutProg[0] {
	case "--help", "-h":
		fmt.Println(os.Args[0], "is called with --list or --host <hostname> and will then return a dynamic inventory that can be used by ansible using -i <this_file>")
	case "--list":
		h.serveList()
	case "--host":
		result := h.serveHost(argsWithoutProg[1])
		if len(result) > 0 {
			h.sendOut(result)
		} else {
			fmt.Println("{}")
			os.Exit(0)
		}
	default:
		panic("Incorrect command line parameters")
	}
}

// serves --host name, returning a map of vars for that given host
func (h *AnsibleInventory) serveHost(host string) map[string]string {
	var vars map[string]string
	host = strings.ToUpper(host)
	for _, v := range h.Hosts {
		if strings.ToUpper(v.Host) == host {
			vars = v.Vars
			break
		}
	}
	return vars
}

// serveList serves --list
func (h *AnsibleInventory) serveList() {
	h.sendOut(h.ProduceOutput())
}

// sendOut marshals to json, prints out and exits program
func (h *AnsibleInventory) sendOut(input interface{}) {
	bytes, err := json.Marshal(&input)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(bytes))
	os.Exit(0)
}

// MakeTestInventory creates an inventory used for testing.
// Changing this inventory can result in that tests also fail.
func MakeTestInventory() (inventory AnsibleInventory) {
	host1 := AnsibleHost{
		Host:   "host1",
		Groups: []string{"servers", "clients"},
		Vars:   map[string]string{"foo": "bar"},
	}
	host2 := AnsibleHost{
		Host:   "host2",
		Groups: nil,
		Vars:   map[string]string{"bee": "honey", "free": "beer"},
	}
	host3 := AnsibleHost{
		Host:   "host3",
		Groups: []string{"clients"},
		Vars:   nil,
	}
	inventory.Hosts = []AnsibleHost{host1, host2, host3}
	inventory.GroupVars = make(map[string]map[string]string)
	inventory.GroupVars["servers"] = map[string]string{"group": "servers"}
	inventory.Groups = make(map[string][]string)
	inventory.Groups["all_servers"] = []string{"servers"}
	return
}

// NewInventoryFromFile load an inventory from file
func NewInventoryFromFile(fn string) (i AnsibleInventory, err error) {
	bytes, err := os.ReadFile(fn)
	if err == nil {
		err = json.Unmarshal(bytes, &i)
	}
	return
}

package ansiblemodule

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"os"
	"strconv"
)

// Example code: https://gist.github.com/sivel/ccd81bdfb31ca0c0e05d

// AnsibleReponse is the response sent back to Ansible after execution
type AnsibleReponse struct {
	Msg         interface{} `json:"msg"`                   // the message that is returned back to Ansible
	Changed     bool        `json:"changed"`               // true if we changed anything
	Failed      bool        `json:"failed"`                // true if this module failed
	Unreachable bool        `json:"unreachable,omitempty"` // true if unreachable was the fail reason
}

// AnsibleRequest is the parameters passed from Ansible
type AnsibleRequest map[string]interface{}

// GetKeyAsInt tries to get a number from a key, returns 0 otherwise
func (a AnsibleRequest) GetKeyAsInt(key string) int {
	if val, ok := a[key]; ok {
		switch val.(type) {
		case int:
			return val.(int)
		case int64:
			return int(val.(int64))
		case uint64:
			return int(val.(uint64))
		case float32:
			return int(val.(float32))
		case float64:
			return int(val.(float64))
		case string:
			res, err := strconv.Atoi(val.(string))
			if err != nil {
				return res
			}
		}
	}
	return 0
}

// GetKeyAsString returns the key as a string if possible, and empty string if not possible
func (a AnsibleRequest) GetKeyAsString(key string) string {
	if val, ok := a[key]; ok {
		switch val.(type) {
		case int, float64, float32, uint, int64:
			return fmt.Sprint(val)
		case bool:
			return strconv.FormatBool(val.(bool))
		case string:
			return val.(string)
		}
	}
	return ""
}

// GetKeyAsType tries to unmarshal a key to a given type into result
func (a AnsibleRequest) GetKeyAsType(key string, result interface{}) error {
	if val, ok := a[key]; ok {
		err := mapstructure.Decode(val, &result)
		return err
	}
	return errors.New("No such key")
}

// NewAnsibleRequestFromBytes returns a request from a byte array
func NewAnsibleRequestFromBytes(bytes []byte) (a AnsibleRequest, err error) {
	err = json.Unmarshal(bytes, &a)
	return
}

// NewAnsibleRequestFromFile load configuration from file
func NewAnsibleRequestFromFile(fn string) (a AnsibleRequest, err error) {
	bytes, err := os.ReadFile(fn)
	if err == nil {
		err = json.Unmarshal(bytes, &a)
	}
	return
}

// WriteResponse write response to stdout and exit
func WriteResponse(resp AnsibleReponse) {
	bytes, err := json.Marshal(&resp)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(bytes))
	if resp.Failed {
		os.Exit(1) // TODO: do I need to exit with error code 1 when I am responding with fail???
	} else {
		os.Exit(0)
	}
}

// FailResponse responds with an error and exits the program
func FailResponse(err interface{}) {
	response := AnsibleReponse{
		Msg:     err,
		Changed: false,
		Failed:  true,
	}
	WriteResponse(response)
}

// SuccessResponse sends a success response and exits the program
func SuccessResponse(changed bool, result interface{}) {
	response := AnsibleReponse{
		Msg:     result,
		Changed: changed,
		Failed:  false,
	}
	WriteResponse(response)
}
